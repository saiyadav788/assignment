

//InsertRunTime


package day1_Assignment;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import common.db.DBConnection;

public class InsertRunTime {

	public static void main(String[] args) {
		
		Connection connection = DBConnection.getConnection();
		Statement statement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmpID : ");
		int empId = scanner.nextInt();
		System.out.println("Enter EmpName : ");
		String empName = scanner.next();
		System.out.println("Enter Salary : ");
		double salary = scanner.nextDouble();
		System.out.println("Enter Gender : ");
		String gender = scanner.next();
		System.out.println("Enter EmailID : ");
		String emailId = scanner.next();
		System.out.println("Enter Password : ");
		String password = scanner.next();
		
		String insertQuery = "insert into employee value ( "
				+ empId+", '"+empName+"',"+ salary+", '"
				+gender+"', '"+emailId+"', '"+password+"')";
		
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			if(result > 0){
				System.out.println(result + " Record(s) Inserted...");
			}else{
				System.out.println("Records Insertion Failed......");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			if(connection != null){
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

}
***********************************************************************


//FetchFromLocalStorage



package day1_Assignment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import common.db.DBConnection;

public class FetchFromLocalStorage {

	public static void main(String[] args) {
		Connection connection = DBConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		ArrayList<Integer> empIDs = new ArrayList<>();
		
		String selectQuery = "select empId from employee";
		try {
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(selectQuery);
			if (resultSet != null){
				while(resultSet.next()){
//					System.out.print(resultSet.getInt(1) + " ");
					empIDs.add(resultSet.getInt(1));
				}
				Collections.sort(empIDs);
				System.out.println(empIDs);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmpID :");
		int empId = scanner.nextInt();
		
		String selectQuery2 = "select * from employee where empId = " + empId;
		
		ResultSet resultSet2 = null;
		
		try {
			resultSet2 = statement.executeQuery(selectQuery2);
			if (resultSet2 != null){
				resultSet2.next();
				System.out.println("EmpID : "+ resultSet2.getInt(1));
				System.out.println("EmpName : "+ resultSet2.getString(2));
				System.out.println("Salary : "+ resultSet2.getDouble(3));
				System.out.println("Gender : "+ resultSet2.getString(4));
				System.out.println("EmailID : "+ resultSet2.getString(5));
				System.out.println("PAssword : "+ resultSet2.getString(6));
			}else{
				System.out.println("Enter VAlid EmpID.....");
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		
		try {
			if(connection != null){
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}

*****************************************************************


//FetchByEmpId




package day1_Assignment;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import common.db.DBConnection;

public class FetchByEmpId {

	public static void main(String[] args) {
		Connection connection = DBConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter EmployeeID to Get Details : ");
		int empId = scanner.nextInt();
		
		String selectQuery = "select * from employee where empId = " + empId;
		
		try {
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(selectQuery);
			
			if(resultSet != null){
				resultSet.next();
				
				System.out.println("empId : "+resultSet.getInt(1));
				System.out.println("EmpName : "+ resultSet.getString(2));
		}else{
				System.out.println("Record not Found in the Table");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if(connection != null){	
				resultSet.close();
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}			System.out.println("Salary : "+ resultSet.getDouble(3));
				System.out.println("Gender : "+ resultSet.getString(4));
				System.out.println("EmailID : "+ resultSet.getString(5));
				System.out.println("Password : "+ resultSet.getString(6));
		

